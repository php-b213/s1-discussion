<?php
	//Single Line Comments [ctrl + /]
	/* 
		Multi-Line Comments [ctrl + shift + /]
	*/ 

	//Variables -> used to contain/store data.
		/* Variables in PHP are defined using dollar ($) notation before the name of the variable. Ex: $varName; 
			Always end your statements with semi-colon.
		*/
		$name = 'Casi';
		$email = 'casi@mail.com';

//Constants
		//Constants in PHP are defined using the define() function
		define('PI', 3.1416);

//Data Types
		$state = 'New York';
		$country = 'United States of America';

//Integers
		$age = 21;
		$headcount = 27;

//Floating points
		$grade = 98.2;
		$distanceInKilometers = 526.28;

//Boolean
		$hasTraveledAbroad = true;
		$isSingle = false;

//Null
		$spouse = null;

//Arrays
		$myGrades = array(98.2, 93.1, 96.4, 95.2);

//Objects
		$gradesObj = (object)[
			'firstGrading' => 95.4,
			'secondGrading' => 97.5,
			'thirdGrading' => 94.5,
			'fourthGrading' => 98.7
		];

//Nested Object
		$personObj = (object)[
			'fullName' => 'Lara Croft',
			'isMarried' => false,
			'age' => 49,
			'address' => (object)[
				'state' => 'New York',
				'country' => 'USA'
			]
		];


//Operators
		//Assignment operators are used to assign values to variables (=)
		$x = 1342.14;
		$y = 1268.24;

//Logical Operators
		$isLegalAge = true;
		$isRegistered = false;

//Function
		function getFullName($firstName, $middleInitial, $lastName) {
			return "$lastName, $firstName $middleInitial";
		}


//SELECTION CONTROL STRUCTURES
		//If-ElseIf-Else statement
		function determineTyphoonIntensity($windspeed){
			if($windspeed < 30) {
				return 'Not a typhoon yet';
			} 
			else if ($windspeed <= 61) {
				return 'Tropical Depression detected';
			}
			else if ($windspeed >= 62 && $windspeed <= 88) {
				return 'Tropical Storm detected';
			}
			else if ($windspeed >= 89 && $windspeed <= 117) {
				return 'Severe Tropical Storm detected';
			}
			else {
				return 'Typhoon detected';
			}

		}


//SWITCH STATEMENT
	function determineComputerUser($computerNumber){
		switch ($computerNumber){
			case 1: 
				return 'Linus Trovalds';
				break;
			case 2: 
				return 'Steve Jobs';
				break;
			case 3:
				return 'Sid Meier';
				break;
			case 4:
				return 'Onel De Guzman';
				break;
			case 5:
				return 'Christian Salvador';
				break;
			default:
				return $computerNumber . ' is out of bounds.';
		}
	}


//Ternary Operator
	function isUnderAge($age){
		return ($age < 18) ? true : false;
	}

//Try-Catch-Finally
	function greeting($str){
		try {
			//attempt to execute a code
			if(gettype($str) == "string"){
				echo $str;
			}
			else {
				throw new Exception("Hello");
			}
		}
		catch (Exception $e){
			echo $e->getMessage();
		} 
		finally {
			//Continue execution of code regardless of success or failure of code execution in 'try' block.
			echo " I did it again!";
		}
	}
?>