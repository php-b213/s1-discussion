<?php require_once "./code.php"?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S1: PHP Basics & Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>
	<!-- Use "double quotes" when echoing a statement with a variable. It will not be recognized by the browser if you use single quotes. -->
	<p><?php echo "Good day, $name! Your provided email is: $email. Your age is $age."; ?></p>

	<!-- When we declare a constant variable, we don't need a "$" symbol. -->
	<p><?php echo "The value of Pi is " . PI . "."; ?></p>
	<p><?php echo $hasTraveledAbroad ?></p>
	<p><?php echo $isSingle ?></p>

	<p><?php echo gettype($hasTraveledAbroad) ?></p>
	<!-- To dump information about a variable, use the var_dump in PHP. This function provides structured data about the specified variable, including its type and value. -->
		<p><?php echo var_dump($isSingle) ?></p>

	<p><?php echo $spouse ?></p>

	<p><?php echo var_dump($spouse); ?></p>

	<p><?php echo $myGrades[3]; ?></p>
	<p><?php echo $myGrades[1]; ?></p>

	<!-- Arrow Function => points out the specific object we are accessing. -->
	<p><?php echo $gradesObj->firstGrading ?></p>

	<p><?php echo $personObj->address->state ?></p>

	<h1>Operators</h1>
	<p>X: <?php echo $x ?></p>
	<p>Y: <?php echo $y ?></p>

	<h2>Arithmetic Operators</h2>
	<p> Sum: <?php echo $x + $y; ?> </p>
	<p> Difference: <?php echo $x - $y; ?> </p>
	<p> Product: <?php echo $x * $y; ?> </p>
	<p> Quotient: <?php echo $x / $y; ?> </p>

	<h2>Equality Operators</h2>
	<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '1342.14'); ?></p>
	<p>Loose Inequality: <?php echo var_dump($x != '1342.14'); ?></p>
	<p>Strict Inequality:<?php echo var_dump($x !== '1342.14'); ?></p>


	<h2>Greater Than/Lesser Than Operators</h2>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>


	<h2>Logical Operator</h2>
	<p>Are All Requirements Met?: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met?: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are All Requirements Met?: <?php echo var_dump($isLegalAge && !$isRegistered); ?></p>


	<h2>Function</h2>
	<!-- Invoke the function -->
	<p> Full Name: <?php echo getFullName('John', 'D.', 'Smith') ?>

	<h2>If-else-if-else statement</h2>
	<p><?php echo determineTyphoonIntensity(12) ?></p>

 	<h2>Switch Statement</h2>
 	<p><?php echo determineComputerUser(5)?></p>

 	<h2>Ternary Operator Sample</h2>
 	<p>78: <?php var_dump(isUnderAge(78)) ?></p>

 	<h2>Try Catch Finally</h2>
 	<p><?php echo greeting(12)?></p>
</body>
</html>